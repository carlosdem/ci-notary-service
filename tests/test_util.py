# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import pytest

from sftpnotary import util
from sftpnotary.exceptions import Error, InvalidFileName


class TestUtil:
    def test_collectArgumentsUpToSize(self):
        assert list(util.collectArgumentsUpToSize(["1.exe"], 42)) == [["1.exe"]]
        assert list(util.collectArgumentsUpToSize(["1.exe"], 5)) == [["1.exe"]]
        with pytest.raises(Error):
            list(util.collectArgumentsUpToSize(["1.exe"], 4))

        assert list(util.collectArgumentsUpToSize(["1.exe", "11.exe"], 42)) == [["1.exe", "11.exe"]]
        assert list(util.collectArgumentsUpToSize(["1.exe", "11.exe"], 6)) == [["1.exe"], ["11.exe"]]
        with pytest.raises(Error):
            list(util.collectArgumentsUpToSize(["1.exe", "11.exe"], 5))

        assert list(util.collectArgumentsUpToSize(["1.exe", "2.exe", "3.exe"], 11)) == [["1.exe", "2.exe"], ["3.exe"]]
        assert list(util.collectArgumentsUpToSize(["1.exe", "2.exe", "3.exe"], 10)) == [["1.exe"], ["2.exe"], ["3.exe"]]

    def test_validateFileName(self):
        util.validateFileName("foo.apk")

        with pytest.raises(InvalidFileName):
            util.validateFileName(None)
        with pytest.raises(InvalidFileName):
            util.validateFileName("")
        with pytest.raises(InvalidFileName):
            util.validateFileName("../../etc/passwd")

        util.validateFileName("foo bar.apk")
        util.validateFileName("foo\tbar.apk")
        util.validateFileName("föö.apk")

    def test_validateFileName_strict(self):
        util.validateFileName("foo-arm64-v8a.apk", strict=True)

        with pytest.raises(InvalidFileName):
            util.validateFileName(None, strict=True)
        with pytest.raises(InvalidFileName):
            util.validateFileName("", strict=True)
        with pytest.raises(InvalidFileName):
            util.validateFileName("../../etc/passwd", strict=True)

        with pytest.raises(InvalidFileName):
            util.validateFileName("foo bar.apk", strict=True)
        with pytest.raises(InvalidFileName):
            util.validateFileName("foo\tbar.apk", strict=True)
        with pytest.raises(InvalidFileName):
            util.validateFileName("föö.apk", strict=True)
