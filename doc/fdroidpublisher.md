<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# fdroidpublisher.py - An F-Droid Publishing Service

This services publishs APKs in F-Droid repositories.

## Prerequisites

The service needs an SFTP server for exchanging data with the clients.

## Installation

### Install an Android SDK

`fdroidpublisher.py` and the [fdroidserver](https://gitlab.com/fdroid/fdroidserver)
tools need the tools `apksigner` and `apkanalyzer` from an Android SDK.
The following instructions are based on the installation of
the Android SDK in KDE's Android Docker image. See below for an alternative
setup using KDE's Android Docker image.

```sh
export NDK_VERSION=22.1.7171670
export SDK_PLATFORM=android-33
export SDK_BUILD_TOOLS=30.0.3
export SDK_PACKAGES="tools platform-tools"

export ANDROID_HOME=~/android-sdk
export ANDROID_SDK_ROOT=${ANDROID_HOME}
export PATH=${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}

curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip'
mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools
unzip -q /tmp/sdk-tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools
rm -f /tmp/sdk-tools.zip

yes | sdkmanager --licenses
sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" "ndk;${NDK_VERSION}" ${SDK_PACKAGES}
sdkmanager --uninstall --verbose emulator
```

### Install the Service

Clone this repository on a server that shall provide the service,
create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git fdroidpublisher
cd fdroidpublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements_fdroid.txt
```

### Configure the Service

Copy `fdroidpublisher.sample.ini` to `fdroidpublisher.ini` and replace the example values
with the appropriate values for your installation.

Then copy `fdroidpublisher-projects.sample.yaml` to `fdroidpublisher-projects.yaml` and
specify the settings for the projects you want to publish APKs for.

### Set up an F-Droid App Repo

Very brief list of commands to run:

```sh
export ANDROID_HOME=~/android-sdk

mkdir -p .../fdroid-repos/nightly/fdroid
cd .../fdroid-repos/nightly/fdroid
fdroid init
```

Edit the `config.yml` file to your liking. In particular, set `serverwebroot`.

For details read the official instructions to
[set up an F-Droid App Repo](https://f-droid.org/en/docs/Setup_an_F-Droid_App_Repo/).

## Run the Service

Activate the virtual environment and start `fdroidpublisher.py`.

```sh
cd ~/fdroidpublisher
. .venv/bin/activate
python3 fdroidpublisher.py --id workerA --config fdroidpublisher.ini
```

where you replace `workerA` with a unique alpha-numeric ID for each instance
of `fdroidpublisher.py` you start.

## Use the Service to Publish an APK

Clone this repository on the client, create a virtual environment, and install
the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git fdroidpublisher
cd fdroidpublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

Copy `publishonfdroid.sample.ini` to `publishonfdroid.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `publishonfdroid.py` to publish a signed APK

```sh
python3 publishonfdroid.py --config publishonfdroid.ini --fastlane <fastlane> <apk>...
```
where `<apk>...` are the APKs you want to publish and `<fastlane>` is the
fastlane zip file containing the meta data for the APKs.

## Manage the Service with systemd

We set up the service under the user's control with a per-user systemd instance.

Copy `fdroidpublisher-user.sample.service` to `~/.config/systemd/user/fdroidpublisher-user.service`
and modify it as needed. The example uses the username as ID of the service
worker. Make sure that the ID is unique if you run multiple service workers.

Start the service with
```sh
systemctl --user start fdroidpublisher-user.service
```

If you want the service to be started automatically, then enable autostart with
```sh
systemctl --user enable fdroidpublisher-user.service
```

Note: By default, the systemd user instance and thus the user-managed services are
started after the first login of a user and killed after the last session of the
user is closed. To start the user instance right after boot, and keep the systemd
user instance running after the last session closes you have to enable lingering
for the user:
```sh
loginctl enable-linger USERNAME
```

For more details on user-managed services see https://wiki.archlinux.org/title/Systemd/User.
