<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileCopyrightText: 2023 Ben Cooksley <bcooksley@kde.org>
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# websitepublisher.py - A service to publish files securely to a either a local or remote location (usually for serving by a web server)

This service takes an archive of a website, unpacks it and transfers the contents
using rsync to a specified destination, which can be either local or remote.

## Prerequisites

The service needs an SFTP server for exchanging data with the clients.

Additionally `rsync` must be installed and available in path, as it is used
to transfer files to the final destination, whether local or remote.

For transfers to remote destinations, an SSH private key pair must be created
and authorised to access the remote system. The key pair must be available for
use without a password - either because the key has no password or because it
has been added to an SSH agent prior to the service being started. Use of SSH
password based authentication is not supported.

Should particular parameters (such as a non-standard port) be required to connect
to the remote system then this must be configured in ~/.ssh/config

## Installation

### Install the Service

Clone this repository on a server that shall provide the service,
create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git websitepublisher
cd websitepublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

### Configure the Service

Copy `websitepublisher.sample.ini` to `websitepublisher.ini` and replace the example values
with the appropriate values for your installation.

Then copy `websitepublisher-projects.sample.yaml` to `websitepublisher-projects.yaml` and
specify the settings for the projects you want to use service for.

## Run the Service

Activate the virtual environment and start `websitepublisher.py`.

```sh
cd ~/websitepublisher
. .venv/bin/activate
python3 websitepublisher.py --id workerA --config websitepublisher.ini
```

where you replace `workerA` with a unique alpha-numeric ID for each instance
of `websitepublisher.py` you start. You can run multiple instances, but the
behavior is undefined if multiple instances operate on the same destination.
Therefore, it's recommended to run only one service for a given Gitlab repository.

## Use the Service to Publish a Website

Clone this repository on the client, create a virtual environment, and install
the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git websitepublisher
cd websitepublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

Copy `publishwebsite.sample.ini` to `publishwebsite.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `publishwebsite.py` to sign and publish the website.

```sh
python3 publishwebsite.py --config publishwebsite.ini <path to files to be published remotely>
```
