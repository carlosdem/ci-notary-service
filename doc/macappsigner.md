<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
# SPDX-FileCopyrightText: 2023 Julius Künzel <jk.kdedev@smartlab.uber.space>
-->

# macappsigner.py - A Signing Service for macOS apps

Provides code signing for macOS apps.

`*.dmg` files can by signed directly, `*.app` files are rather special folders than real files and should be packed inside a tar archive for signing.

## Prerequisites

The service needs an SFTP server for exchanging data with the clients.

Additionally, for signing your macOS app you need
* a key store file containing the certificate to sign the app with,
* a file containing the password to unlock the key store.

## Installation

### Install dependencies

Install `rcodesign` as described here in the documentation: https://gregoryszorc.com/docs/apple-codesign/0.26.0/apple_codesign_getting_started.html#installing

Install a recent version of `7zz` either from your distros package manager or from https://7-zip.org/download.html

### Install the Service

Clone this repository on a server that shall provide the service,
create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git macappsigner
cd macappsigner
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

### Configure the Service

Copy `macappsigner.sample.ini` to `macappsigner.ini` and replace the example values
with the appropriate values for your installation.

Then copy `macappsigner-projects.sample.yaml` to `macappsigner-projects.yaml` and
specify the settings for the projects you want to sign apps for.

## Run the Service

Activate the virtual environment and start `macappsigner.py`.

```sh
cd ~/macappsigner
. .venv/bin/activate
python3 macappsigner.py --id workerA --config macappsigner.ini
```

where you replace `workerA` with a unique alpha-numeric ID for each instance
of `macappsigner.py` you start.

## Use the Service to Sign a macOS App

Clone this repository on the client, create a virtual environment, and install
the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git macappsigner
cd macappsigner
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

Copy `signmacapp.sample.ini` to `signmacapp.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `signmacapp.py` to sign the macOS app

```sh
python3 signmacapp.py --config signmacapp.ini <appfile>
```

## Managing the Service with systemd

We set up the service under the user's control with a per-user systemd instance.

Copy `macappsigner-user.sample.service` to `~/.config/systemd/user/macappsigner-user.service`
and modify it as needed. The example uses the username as ID of the service
worker. Make sure that the ID is unique if you run multiple service workers.

Start the service with
```sh
systemctl --user start macappsigner-user.service
```

If you want the service to be started automatically, then enable autostart with
```sh
systemctl --user enable macappsigner-user.service
```

Note: By default, the systemd user instance and thus the user-managed services are
started after the first login of a user and killed after the last session of the
user is closed. To start the user instance right after boot, and keep the systemd
user instance running after the last session closes you have to enable lingering
for the user:
```sh
loginctl enable-linger USERNAME
```

For more details on user-managed services see https://wiki.archlinux.org/title/Systemd/User.
