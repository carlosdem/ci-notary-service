# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import logging
from pathlib import Path
from typing import Optional

from sftpnotary.core import Job
from sftpnotary.microsoftstoredata import PublishOnMicrosoftStoreRequest, PublishOnMicrosoftStoreResponse
from sftpnotary.sftp import SFTPClient, SFTPError

log = logging.getLogger(__name__)


class PublishOnMicrosoftStoreJob(Job):
    def __init__(
        self,
        sftp: SFTPClient,
        appxFilePath: Path,
        token: Optional[str] = None,
        projectPath: Optional[str] = None,
        branch: Optional[str] = None,
    ):
        super().__init__(sftp)

        self.appxFilePath = appxFilePath
        self.request = PublishOnMicrosoftStoreRequest(
            fileName=None,
            token=token,
            projectPath=projectPath,
            branch=branch,
        )
        self.resultPath = self.appxFilePath.parent

    def start(self):
        appxFileName = self.appxFilePath.name
        self.sftp.upload(self.appxFilePath, self.task.path() / appxFileName)
        self.request.fileName = appxFileName
        self.task.putRequest(self.request)
        self.taskQueue.commitTask(self.task)

    def waitForCompletion(self):
        self.taskQueue.waitForCompletion(self.task)
        log.debug(f"Fetching response of task '{self.task.id}' ...")
        response = None
        try:
            response = self.task.fetchResponse(PublishOnMicrosoftStoreResponse)
        except SFTPError as e:
            log.error("Error: %s", e)
        if response is None:
            log.error("Error: Got no response. Task failed.")
        else:
            self.success = True
        self.downloadLogFiles(self.resultPath)
        self.taskQueue.removeTask(self.task)
