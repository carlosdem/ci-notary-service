# -*- coding: utf-8 -*-
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2014 Hannah von Reth <vonreth@kde.org>

# central instance for managing settings regarding craft

import configparser
import os
import sys
from pathlib import Path
from typing import List, Optional, Union


def loadConfig(iniPath):
    global settings
    settings = Config(iniPath)


class Config:
    def __init__(self, iniPath=None):
        self._config = configparser.ConfigParser(interpolation=None)
        if iniPath:
            self.iniPath = Path(iniPath)
            self._readSettings()

    def _readSettings(self):
        if not os.path.exists(self.iniPath):
            print("Could not find config: %s" % self.iniPath, file=sys.stderr)
            return

        self._config.read(self.iniPath, encoding="utf-8")

    def __contains__(self, key):
        return self._config and self._config.has_option(*key)

    def get(self, group, key, default=None):
        return self._config.get(group, key, fallback=default)

    @staticmethod
    def _parseList(s: str) -> List[str]:
        return [v.strip() for v in s.split(";") if v]

    def getList(self, group, key, default=None):
        return Config._parseList(self.get(group, key, default))

    def getPath(self, group: str, key: str, default: Optional[Union[Path, str]] = None) -> Optional[Path]:
        v = self.get(group, key, default)
        if isinstance(v, str):
            v = Path(v).expanduser()
        return v

    def getSection(self, group):
        if self._config.has_section(group):
            return self._config.items(group)
        else:
            return []

    def getboolean(self, group, key, default=False):
        return self._config.getboolean(group, key, fallback=default)

    def set(self, group, key, value):
        if value is None:
            return
        if not self._config.has_section(group):
            self._config.add_section(group)
        self._config[group][key] = str(value)

    def setDefault(self, group, key, value):
        if (group, key) not in self:
            self.set(group, key, value)


settings = Config()
