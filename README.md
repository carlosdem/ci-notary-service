<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# CI Notary Service

This project provides code signing services, publishing services, and other
services that require sensitive data for usage by our CI.

## Basic Idea

Servers providing certain services and clients that want to use those services
exchange requests and responses via an SFTP server. This way the service
providers do not need to be reachable via a public IP address.

If you want to learn more read the [Technical Details](doc/details.md) page.

## Services

The following services are provided:
* [aabsigner](doc/aabsigner.md) - signs Android app bundles (AAB)
* [apksigner](doc/apksigner.md) - signs APKs
* [fdroidpublisher](doc/fdroidpublisher.md) - publishes APKs to F-Droid repositories
* [flatpaksigner](doc/flatpaksigner.md) - adds Flatpak bundles to a repository signing the commits
* [googleplaypublisher](doc/googleplaypublisher.md) - publishes APKs in Google Play
* [macappnotarizer](doc/macappnotarizer.md) - sends a notarization request to Apple to notarize DMGs
* [macappsigner](doc/macappsigner.md) - signs .app/DMGs
* [microsoftstorepublisher](doc/microsoftstorepublisher.md) - publishes APPXs in Microsoft Store
* [websitepublisher](doc/websitepublisher.md) - publishes static websites to webservers
* [windowsbinariessigner](doc/windowsbinariessigner.md) - signs Windows binaries and packages (APPX/MSIX)


## Installation

Clone this repository, create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

## Local testing

Patch the python file of the service like

```diff
-    startWorker(options.workerId)
+    # startWorker(options.workerId)
+    from unittest.mock import patch
+
+    fakeJob = {"ref": "master", "pipeline": {"project_id": 73, "source": "push"}}
+    with patch("sftpnotary.authentication.GitLabJobTokenAuthentication.getJob", return_value=fakeJob):
+        startWorker(options.workerId)
```

to fake the `/job` request done to invent with the job token.

A SFTP server is the core part of all services.

A simple way to get one runnning is the `lscr.io/linuxserver/openssh-server` docker. You can start it like this:

```bash
docker run -d \
    --name=openssh-server \
    -e PUID=1000 \
    -e PGID=1000 \
    -e TZ=Etc/UTC \
    -e USER_NAME=kde \
    -p 2222:2222 \
    -e PUBLIC_KEY="ssh-rsa A...z== something@something" \ # Replace this by your own public key
    --restart unless-stopped \
    lscr.io/linuxserver/openssh-server:latest
```

With the above docker the `[SFTP]` section of your config needs to look like this:

```ini
[SFTP]
Server = 0.0.0.0
Port = 2222
Username = kde
# This depends on the service you are testing
BasePath = websitepublisher
RsaKeyFile = /path/to/private/ssh_key
```

If you run the service also inside a Docker container then you need to use `Server = host.docker.internal` as SFTP server for the service.

The private SSH key should not be encrypted / password protected.

Note: paramiko's error messages are sometimes wrong and misleading. Eg. if the port in your config is wrong, it might tell you that the key is encrypted.

For running the client you need to pass a few environment variables, e.g.

```shell
CI_JOB_TOKEN=dummy \ # This does not need to be an actual job token, can be anything
CI_PROJECT_PATH=pim/itinerary \
CI_COMMIT_REF_NAME=master \
```

`CI_PROJECT_PATH` and `CI_COMMIT_REF_NAME` must match `project_id` and `ref` used for fakeJob (see patch above).
